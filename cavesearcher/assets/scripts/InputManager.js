const GameController = require('GameController');

cc.Class({
    extends: cc.Component,

    properties: {
        playerRigidBody : cc.RigidBody,
        upForce : 1000,
        tapToStartLabel : cc.Node,
        gameController : GameController,
    },


    onLoad () {
        this.node.on('touchstart', () => {
            if (!this.firstTimePressed) {
                console.log("tap to start");
                this.firstTimePressed = true;
                //this.playerRigidBody.gravityScale = 1.43;
                this.gameController.startGame();
            }
            else{
                this.isInputPressed = true;
            }
        }, this.node);

        this.node.on('touchend', () => {
            this.isInputPressed = false;
        }, this.node);

    },

    start () {
        this.firstTimePressed = false;
        this.isInputPressed = false;
    },

    update (dt) {
        //console.log(this.isInputPressed);
        if (this.isInputPressed) {
            this.playerRigidBody.applyForceToCenter(new cc.Vec2(0,this.upForce));
        }
    },
});
