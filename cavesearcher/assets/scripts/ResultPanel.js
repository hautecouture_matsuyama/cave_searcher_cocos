cc.Class({
    extends: cc.Component,

    properties: {
        homeButton: cc.Button,
        retryButton: cc.Button,
        videoButton: cc.Button,
        screenSaver: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},
    
    home() {
        var anim = this.homeButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            var screenAnim = this.screenSaver.getComponent(cc.Animation);
            screenAnim.play('fadeOut');
        }, 0.5);

        this.scheduleOnce(() => {
            cc.director.loadScene("menu");
        }, 0.7);
    },

    retry() {
        var anim = this.retryButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            var screenAnim = this.screenSaver.getComponent(cc.Animation);
            screenAnim.play('fadeOut');
        }, 0.5);

        this.scheduleOnce(() => {
            cc.director.loadScene("game");
        }, 0.7);
    },

    video() {
        var anim = this.videoButton.getComponent(cc.Animation);
        anim.play('buttonFlash');
    },
});
