cc.Class({
    extends: cc.Component,

    properties: {
        startButton: cc.Button,
        rankingButton: cc.Button,
        screenSaver: cc.Node,
        leaderBoard: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },

    // update (dt) {},
    // 
    playGame() {
        var anim = this.startButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            var screenAnim = this.screenSaver.getComponent(cc.Animation);
            screenAnim.play('fadeOut');
        }, 0.5);

        this.scheduleOnce(() => {
            cc.director.loadScene("game");
        }, 0.8);
    },

    showLeaderboard () {
        var anim = this.rankingButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            this.leaderBoard.active = true;
        }, 0.5);
    }
});
