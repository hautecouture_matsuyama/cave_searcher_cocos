const GameController = require('GameController');

cc.Class({
    extends: cc.Component,

    properties: {
        audio : {
            default : null,
            url : cc.AudioClip,
        },
        gameController : GameController,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        this.score = 0;
    },

    // update (dt) {},
    // 
    onCollisionEnter (other, self) {
        var bonus = 1;
        if (cc.sys.localStorage.setItem('bonus', true)) {
            bonus = 2;
        }
        this.score += 1 * bonus;
        //console.log("score: " + this.score);
        this.gameController.setScoreText(this.score);
        cc.audioEngine.play(this.audio, false, 1);
    },

    getScore() {
        return this.score;
    },
});
