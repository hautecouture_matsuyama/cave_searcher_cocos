cc.Class({
    extends: cc.Component,

    properties: {
    	speed : 100,
    },

    // onLoad () {},

    start () {
    	this.currentSpeed = 0;
    },

    update (dt) {
        this.node.x += -this.currentSpeed * dt;
    },

    startMoving () {
        this.currentSpeed = this.speed;
    },

    stopMoving() {
        this.currentSpeed = 0;
    },
});
