//var GameController = require('GameController');

cc.Class({
    extends: cc.Component,

    properties: {
        //gameController: GameController,
        speed: 2000,
        isAlive: {
            default: true,
        },
        gravityScale: 1.43,
    },

    // LIFE-CYCLE CALLBACKS:
    onLoad() {
        cc.director.getCollisionManager().enabled = true;
    },

    //start () {},

    //update (dt) {},

    onCollisionEnter (other, self) {
        //console.log("dead");
        this.isAlive = false;
    },

    activateGravity () {
        //console.log("gravity activated");
        var rigidBody = this.node.getComponent(cc.RigidBody);
        rigidBody.gravityScale = this.gravityScale;
    },

    
    
});
