const Player = require('Player');

cc.Class({
    extends: cc.Component,

    properties: {
        player : Player
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {},

    start () {
        this.offsetX = this.node.x - this.player.node.x - 1;
        console.log(this.offsetX);
    },

    update (dt) {
        if (this.player.isAlive) {
            this.node.x += 2;
            //this.node.x = this.player.node.x + this.offsetX;
            console.log(this.node.position);
        }
    },
});
