var Player = require('Player');
var SpikeHolder = require('SpikeHolder');
var BackgroundMovement = require('BackgroundMovement');

cc.Class({
    extends: cc.Component,

    properties: {
        tapToStartLabel : cc.Node,
        player : Player,
        spikeHolders: {
            default: [],
            type: SpikeHolder,
        },
        background: {
            default: [],
            type: BackgroundMovement,
        },
        score : cc.Label,
        pauseMenu : cc.Node,
        resultPanel : cc.Node,
        videoAdPanel : cc.Node,
        endScore : cc.Label,
        bestScore : cc.Label,
        gameOverAudio : {
            default : null,
            url : cc.AudioClip,
        },
        newHighScoreAudio : {
            default : null,
            url : cc.AudioClip,
        },
        pauseButtonClip : {
            default : null,
            url : cc.AudioClip,
        },
        homeButton : cc.Button,
        closeButton : cc.Button,
        pauseButton : cc.Button,
        rankingButton : cc.Node,
        screenSaver : cc.Node,
        leaderBoard : cc.Node,
    },

    onLoad () {
        this.preloadedInterstitial = null;

        if (window['FBInstant']) {
            FBInstant.getInterstitialAdAsync(
                '540248876384921_540249776384831', // Your Ad Placement Id
            ).then(function(interstitial) {
                // Load the Ad asynchronously
                this.preloadedInterstitial = interstitial;
                return preloadedInterstitial.loadAsync();
            }).then(function() {
                console.log('Interstitial preloaded')
            }).catch(function(err){
                console.error('Interstitial failed to preload: ' + err.message);
            });
        }
        else {
            console.log("FBInstant not found");
        }
        
    },

    start () {
        this.playerScore = 0;
        this.isGameOver = false;
    },

    update (dt) {
        if (!this.player.isAlive && !this.isGameOver) {
            this.isGameOver = true;
            this.gameOver();
        }
    },
    
    _startMovement() {
        this.background.forEach((elem) => {
            elem.startMoving();
        });

        this.spikeHolders.forEach((elem) => {
            elem.startMoving();
        });
    },

    startGame() {
        this.tapToStartLabel.active = false;
        this.player.activateGravity();
        
        this._startMovement();
    },

    pauseGame() {
        cc.audioEngine.play(this.pauseButtonClip, false, 1);
        this._stopMovement();
        
        //stop player's movement
        var rigidBody = this.player.getComponent(cc.RigidBody);
        rigidBody.gravityScale = 0.00001;
        rigidBody.linearVelocity = cc.v2();
        
        this.pauseMenu.active = true;
        this.pauseButton.interactable = false;
    },

    unPauseGame() {
        var anim = this.closeButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            this._startMovement();
            this.player.getComponent(cc.RigidBody).gravityScale = 1.43;
            this.pauseMenu.active = false;
            this.pauseButton.interactable = true;
        }, 0.5);
    },

    returnHome() {
        var anim = this.homeButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            var screenAnim = this.screenSaver.getComponent(cc.Animation);
            screenAnim.play('fadeOut');
        }, 0.5);

        this.scheduleOnce(() => {
            cc.director.loadScene("menu");
        }, 0.7);
    },

    setScoreText(score) {
        this.score.string = score;
        this.playerScore = score;
    },

    _stopMovement() {
        this.background.forEach((elem) => {
            elem.stopMoving();
        });

        this.spikeHolders.forEach((elem) => {
            elem.stopMoving();
        });
    },

    gameOver() {
        cc.sys.localStorage.setItem('bonus', false);
        this._stopMovement();
        this.pauseButton.interactable = false;

        //play game over sound clip
        this.node.getComponent(cc.AudioSource).pause();

        //store high score
        var highScore = cc.sys.localStorage.getItem('highScore') ? cc.sys.localStorage.getItem('highScore') : 0;
        
        if(this.playerScore > highScore) {
            highScore = this.playerScore;
            cc.audioEngine.play(this.newHighScoreAudio, false, 0.8);
            cc.sys.localStorage.setItem('highScore', highScore);
        }
        else {
            cc.audioEngine.play(this.gameOverAudio, false, 0.8);
        }

        //set score and highscore to labels
        this.endScore.string = this.playerScore;
        this.bestScore.string = highScore;
        this.leaderBoard.getComponent('Leaderboard').sendScore(this.playerScore);

        //show result panel after sound clip is finished
        this.scheduleOnce(() => {
            this.resultPanel.active = true;
            this.videoAdPanel.active = true;
        }, 0.5);

        //show interstitial ad
        if (Math.random() <= 0.4) {
            console.log("show interstitial");
            if (this.preloadedInterstitial) {
                this.preloadedInterstitial.showAsync()
                .then(function() {
                  // Perform post-ad success operation
                    console.log('Interstitial ad finished successfully');        
                })
                .catch(function(e) {
                    console.error(e.message);
                });
            }
        }
    },

    showRanking() {
        var anim = this.rankingButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        //show ranking
        this.scheduleOnce(() => {
            this.leaderBoard.active = true;
        }, 0.5);
    },
});
