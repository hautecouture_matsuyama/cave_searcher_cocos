cc.Class({
    extends: cc.Component,

    properties: {
        spikeMinY: 327,
        spikeMaxY: 447,
        spikeHolders: {
            default: [],
            type: cc.Node,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getCollisionManager().enabled = true;
    },

    start () {
        this.spikeHolders.forEach((elem) => {
            let y = this._getRandom(this.spikeMinY, this.spikeMaxY);
            elem.y = y + this._getRandom(-4, 15);
        });
    },

    // update (dt) {},
     
    onCollisionEnter (other, self) {
        other.node.x += 2000;
        other.node.y = this._getRandom(this.spikeMinY, this.spikeMaxY);
    },

    _getRandom(min, max) {
      return Math.round(Math.random() * (max - min) + min);
    }

});
