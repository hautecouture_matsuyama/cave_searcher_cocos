cc.Class({
    extends: cc.Component,

    properties: {
        videoButton : cc.Button,
        closeButton : cc.Button,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.preloadedRewardedVideo = null;

        if (window['FBInstant']) {
            FBInstant.getRewardedVideoAsync(
                '540248876384921_540249976384811', // Your Ad Placement Id
            ).then(function(rewarded) {
                // Load the Ad asynchronously
                preloadedRewardedVideo = rewarded;
                return preloadedRewardedVideo.loadAsync();
            }).then(function() {
                console.log('Rewarded video preloaded')
            }).catch(function(err){
                console.error('Rewarded video failed to preload: ' + err.message);
            });
        }
        else {
            console.log("FBInstant not found");
        }
    },

    start () {

    },

    watchVideo () {
        var anim = this.videoButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            if (this.preloadedRewardedVideo) {
                console.log("show reward video");
                this.preloadedRewardedVideo.showAsync()
                .then(function() {
                    // Perform post-ad success operation
                    cc.sys.localStorage.setItem('bonus', true);
                    this.node.active = true;
                    console.log('Rewarded video watched successfully');        
                })
                .catch(function(e) {
                    console.error(e.message);
                });
            }
        }, 0.5);
        
    },

    closePanel () {
        var anim = this.closeButton.getComponent(cc.Animation);
        anim.play('buttonFlash');

        this.scheduleOnce(() => {
            this.node.active = false;
        }, 0.5);
    },

    // update (dt) {},
});
