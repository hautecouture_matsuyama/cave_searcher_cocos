var Player = require('Player');

cc.Class({
    extends: cc.Component,

    properties: {
        player : Player,
        hitAudio : {
            default : null,
            url : cc.AudioClip,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getCollisionManager().enabled = true;
    },

    start () {

    },

    onCollisionEnter (other, self) {
        cc.director.getCollisionManager().enabled = false;

        this.player.isAlive = false;

        cc.audioEngine.play(this.hitAudio, false, 1);
        
        var anim = this.player.getComponent(cc.Animation);
        anim.play('playerDeath');

    },
    // update (dt) {},
});